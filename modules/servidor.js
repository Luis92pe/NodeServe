//Archivos admitidos en el servidor
var content_types = {
    'js': 'text/javascript',
    'html': 'text/html',
    'css': 'text/css',
    'jpg': 'image/jpg',
    'gif': 'image/gif',
    'png': 'image/png'
};



/**
 * Función para iniciar el servidor
 * @param  {[type]} http [description]
 * @param  {[type]} url  [description]
 * @param  {[type]} fs   [description]
 * @return {[type]}      [description]
 */
function initServe(http, url, fs) {

    http.createServer(function(peticion, respuesta) {

        //Obtenemos los valores de la URL
        var direccion = (url.parse(peticion.url).pathname == '/') ? '/home.html' : url.parse(peticion.url).pathname;
        var ruta = 'sites/' + direccion;
        
        // objeto con la variables GET
        var query = url.parse(peticion.url, true).query;
        // Se obtiene el resultado de la varible GET 'variableget'
        var queryVariable = query.variableget;
        

        // se verifica que el archivo existe con el modulo de fs

        fs.exists(ruta, function(existe) {
            if (existe) {
                //Se lee el contenido de la pagina
                fs.readFile(ruta, function(error, contenido) {
                    if (error) {
                        respuesta.writeHead(500, "text/plain");
                        respuesta.end("error");
                    } else {

                        var extension = ruta.split('.').pop();
                        console.log(extension);
                        var mine = content_types[extension];


                        respuesta.writeHead(200, { "Content-Type": mine });
                        respuesta.end(contenido + queryVariable);
                    }
                });
            } else {
                respuesta.writeHead(500, "text/plain");
                respuesta.end("error");
            }
        });

    }).listen('9321', 'localhost');
    console.log("Servidor iniciado");
}

exports.initServe = initServe;