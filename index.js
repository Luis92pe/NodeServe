var http = require("http");
//Trabajamos con los metodos de la URL
var url = require("url");
//Trabajamos con los archivos
var fs = require("fs");

var serve = require('./modules/servidor');


// Se inicia el servidor
serve.initServe(http, url, fs);